import 'dart:convert';

import 'package:http/http.dart' as http;

class BoardRequestFailure implements Exception {}

class CatalogRequestFailure implements Exception {}

class ThreadRequestFailure implements Exception {}

class FileRequestFailure implements Exception {}

class HtmlRequestFailure implements Exception {}

class FourChanApiClient {
  static const _baseUrlHtml = 'boards.4chan.org';
  static const _baseUrlJson = 'a.4cdn.org';
  static const _baseUrlFile = 'i.4cdn.org';
  static const _jsonHeaders = {'Accept': 'application/json'};

  final http.Client _httpClient;

  FourChanApiClient({http.Client? httpClient})
      : _httpClient = httpClient ?? http.Client();

  Future<Map<String, dynamic>> getBoards() async {
    final boardsRequest = Uri.https(_baseUrlJson, '/boards.json');
    final response =
        await _httpClient.get(boardsRequest, headers: _jsonHeaders);

    if (response.statusCode != 200) {
      throw BoardRequestFailure();
    }
    return jsonDecode(response.body) as Map<String, dynamic>;
  }

  Future<List<dynamic>> getCatalog(String boardName) async {
    final catalogRequest = Uri.https(_baseUrlJson, '/$boardName/catalog.json');
    final response =
        await _httpClient.get(catalogRequest, headers: _jsonHeaders);

    if (response.statusCode != 200) {
      throw CatalogRequestFailure();
    }
    return jsonDecode(response.body) as List<dynamic>;
  }

  Future<Map<String, dynamic>> getThread(String boardName, int threadId) async {
    final threadRequest =
        Uri.https(_baseUrlJson, '/$boardName/thread/$threadId.json');
    final response =
        await _httpClient.get(threadRequest, headers: _jsonHeaders);

    if (response.statusCode != 200) {
      throw ThreadRequestFailure();
    }

    return jsonDecode(response.body) as Map<String, dynamic>;
  }

  String getFileUrl(String boardName, String file) {
    final fileRequest = Uri.https(_baseUrlFile, '/$boardName/$file');
    return fileRequest.toString();
  }

  String getHtmlUrl(String subpath) {
    final htmlRequest = Uri.https(_baseUrlHtml, subpath);
    return htmlRequest.toString();
  }

  String getPostUrl(String boardName, int? threadId, int? postId) {
    if (threadId != null && postId != null && threadId != postId) {
      return '${getHtmlUrl('/$boardName/thread/$threadId')}#p$postId';
    } else if (threadId != null) {
      return getHtmlUrl('/$boardName/thread/$threadId');
    } else if (postId != null) {
      return getHtmlUrl('/$boardName/thread/$postId');
    } else {
      return getHtmlUrl('/$boardName');
    }
  }
}
