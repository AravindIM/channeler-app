import 'package:channeler/data/dataproviders/fourchan_api_client.dart';
import 'package:channeler/data/models/board.dart';
import 'package:channeler/data/models/post.dart';
import 'package:intl/intl.dart';
import 'package:html2md/html2md.dart' as html2md;
import 'package:mime/mime.dart';

class FourChanRepository {
  final FourChanApiClient _fourChanApiClient;
  final pageSize = 10;

  FourChanRepository({FourChanApiClient? fourChanApiClient})
      : _fourChanApiClient = fourChanApiClient ?? FourChanApiClient();

  Post _makePostfromJson(
      {required Map<String, dynamic> json,
      required Board board,
      int? threadId}) {
    const dateFormatString1 = "MM/dd/yy(E)HH:mm:ss";
    const dateFormatString2 = "MM/dd/yy(E)HH:mm";

    final dateFormat1 = DateFormat(dateFormatString1);
    final dateFormat2 = DateFormat(dateFormatString2);
    DateTime timestamp;

    try {
      timestamp = dateFormat1.parse(json['now']);
    } catch (e) {
      timestamp = dateFormat2.parse(json['now']);
    }

    final String filename = '${json['tim']}'.trim();
    final String extension = json['ext'] ?? '';
    final String? attachment = filename.isNotEmpty && extension.isNotEmpty
        ? _fourChanApiClient.getFileUrl(board.name, filename + extension)
        : null;

    /* zero-width space used for enforcing links to be part of parent quote block */
    const zeroWidthSpace = '\u200B';

    /* Regex formats for search replacement in the content */
    final markdownLinkFormat = RegExp(r'\[[^\]]*\]\([^\)]*\)');
    final linkFormat =
        RegExp(r'((https?|ftp):\/\/)?([\w-]{1,256}\.)+\w{2,256}([^\s\]]+)?\/?');
    final nonUrlNumberFormat = RegExp(r'^[^A-Za-z]+$');

    final deadLinkRule = html2md.Rule(
      'deadlink',
      filterFn: (node) {
        if (node.nodeName == 'span' && node.className == 'deadlink') {
          return true;
        }
        return false;
      },
      replacement: (content, node) {
        final String deadLink = node.textContent;
        return '[~~$deadLink~~]()';
      },
    );

    final standardisedImageRule = html2md.Rule(
      'standardImage',
      filterFn: (node) {
        return node.nodeName == 'img';
      },
      replacement: (content, node) {
        final String alt = node.getAttribute('alt') ?? '';
        String src = node.getAttribute('alt') ?? '';
        if (src.startsWith('//')) {
          src = 'https:$src';
        } else if (src.startsWith('/')) {
          src = _fourChanApiClient.getHtmlUrl(src);
        }
        return '<img src="$src" alt="$alt">';
      },
    );

    final standardisedLinkRule = html2md.Rule(
      'standardLink',
      filterFn: (node) {
        return node.nodeName == 'a';
      },
      replacement: (content, node) {
        final String label = node.textContent;
        String href = node.getAttribute('href') ?? '';
        if (href.startsWith('//')) {
          href = 'https:$href';
        } else if (href.startsWith('/')) {
          href = _fourChanApiClient.getHtmlUrl(href);
        }
        return '[$label]($href)';
      },
    );

    String markdownContent = html2md.convert(
      json['com'] ?? '',
      rules: [
        deadLinkRule,
        standardisedImageRule,
        standardisedLinkRule,
      ],
    ).trim();

    List<String> markdownLinks = markdownLinkFormat
        .allMatches(markdownContent)
        .map((match) => match[0] ?? '')
        .toList();

    List<String> contentPortions = markdownContent.split(markdownLinkFormat);

    String content = '';

    for (var i = 0; i < contentPortions.length; i++) {
      /* add portions not containing markdown links after linking the urls */
      content = content +
          contentPortions[i].replaceAllMapped(linkFormat, (match) {
            final String matchedText = match[0] ?? '';
            final List<String> urlPortions = matchedText.split('/');
            /* ignore file names */
            if (urlPortions.length == 1) {
              final String? mimeType = lookupMimeType(urlPortions[0]);
              if (mimeType != null) {
                return matchedText;
              }
            }
            /* ignore numbers seperated by special characters */
            if (nonUrlNumberFormat.hasMatch(matchedText)) {
              return matchedText;
            }
            /* urls without protocol specified at the beginning */
            if (!matchedText.startsWith('https://') &&
                !matchedText.startsWith('http://') &&
                !matchedText.startsWith('ftp://')) {
              return '$zeroWidthSpace[$matchedText](https://$matchedText)';
            }
            return '$zeroWidthSpace[$matchedText]($matchedText)';
          });
      /* join with the markdowned links */
      if (i < markdownLinks.length) {
        content = content + zeroWidthSpace + markdownLinks[i];
      }
    }

    int postId = json['no'] as int;
    String url = _fourChanApiClient.getPostUrl(board.name, threadId, postId);

    return Post(
      id: postId,
      board: board.name,
      thread: threadId,
      username: json['name'] ?? 'Anonymous',
      userid: json['id'] as String?,
      title: html2md.convert(json['sub'] ?? '').trim(),
      content: content,
      timestamp: timestamp,
      attachment: attachment,
      replyCount: json['replies'] ?? 0,
      pinned: (json['sticky'] ?? 0) != 0,
      nsfw: board.nsfw,
      url: url,
    );
  }

  List<Post> getCommentPosts(List<Post> posts, int postId) {
    List<Post> comments = [];
    for (var post in posts) {
      if (post.id == postId || post.content.contains(">>$postId")) {
        comments.add(post);
      }
    }
    return comments;
  }

  List<Post> _makePostsfromCatalog(List<dynamic> catalog, Board board) {
    List<Post> posts = [];
    for (var pageJson in catalog) {
      final pageMap = pageJson as Map<String, dynamic>;
      final threadList = pageMap['threads'] as List<dynamic>;
      for (var postJson in threadList) {
        final post = postJson as Map<String, dynamic>;
        posts.add(_makePostfromJson(json: post, board: board));
      }
    }
    return posts;
  }

  List<Post> _makePostsfromThread(
      Map<String, dynamic> threadJson, Board board, int threadId) {
    List<Post> posts = [];
    Map<int, int> postIndex = {};
    List<dynamic> postList = threadJson['posts'] as List<dynamic>;
    final tagRegex = RegExp(r'>>[\d]+');
    for (var (index, postJson) in postList.indexed) {
      final rawPost = postJson as Map<String, dynamic>;
      final post =
          _makePostfromJson(json: rawPost, board: board, threadId: threadId);
      postIndex[post.id] = index;
      final tagMatches = tagRegex.allMatches(post.content);
      for (final Match m in tagMatches) {
        final String match = m[0]!;
        final replyToId = int.parse(match.substring(2));
        final replyToIndex = postIndex[replyToId];
        if (replyToIndex != null) {
          final replyToPost = posts[replyToIndex];
          posts[replyToIndex] =
              replyToPost.copyWith(replyCount: replyToPost.replyCount + 1);
        }
      }
      posts.add(post);
    }
    return posts;
  }

  Future<List<Board>> getBoards() async {
    final boards = await _fourChanApiClient.getBoards();
    final boardsList = boards['boards'] as List<dynamic>;
    return boardsList.map((json) {
      final board = json as Map<String, dynamic>;
      return Board(
        name: board['board'] as String,
        title: board['title'] as String,
        pages: board['pages'] as int,
        threadsPerPage: board['per_page'] as int,
        nsfw: board['ws_board'] == 0,
      );
    }).toList();
  }

  Future<List<Post>> getCatalogPosts(Board board) async {
    final catalog = await _fourChanApiClient.getCatalog(board.name);
    return _makePostsfromCatalog(catalog, board);
  }

  Future<List<Post>> getThreadPosts(Board board, int threadId) async {
    final thread = await _fourChanApiClient.getThread(board.name, threadId);
    return _makePostsfromThread(thread, board, threadId);
  }
}
