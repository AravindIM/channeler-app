// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

class BoardNotFound implements Exception {}

class Board extends Equatable {
  final String name;
  final String title;
  final int pages;
  final int threadsPerPage;
  final bool nsfw;

  const Board(
      {required this.name,
      required this.title,
      required this.pages,
      required this.threadsPerPage,
      required this.nsfw});

  static Board findBoardByName(List<Board> boards, String boardName) {
    Board board;
    try {
      board = boards.firstWhere((board) => board.name == boardName);
    } on StateError catch (_) {
      throw BoardNotFound();
    }
    return board;
  }

  @override
  List<Object> get props {
    return [name, title, pages, threadsPerPage, nsfw];
  }
}
