// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

class Post extends Equatable {
  final int id;
  final String board;
  final int thread;
  final String username;
  final String? userid;
  final String title;
  final String content;
  final DateTime timestamp;
  final String? attachment;
  final int replyCount;
  final bool pinned;
  final bool nsfw;
  final String url;

  const Post({
    required this.id,
    required this.board,
    int? thread,
    required this.username,
    this.userid,
    required this.title,
    required this.content,
    required this.timestamp,
    this.attachment,
    required this.replyCount,
    required this.pinned,
    required this.nsfw,
    required this.url,
  }) : thread = thread ?? id;

  @override
  List<Object> get props {
    return [
      id,
      board,
      thread,
      username,
      title,
      content,
      timestamp,
      replyCount,
      pinned,
      nsfw,
      url,
    ];
  }

  Post copyWith({
    int? id,
    String? board,
    int? thread,
    String? username,
    String? userid,
    String? title,
    String? content,
    DateTime? timestamp,
    String? attachment,
    int? replyCount,
    bool? pinned,
    bool? nsfw,
    String? url,
  }) {
    return Post(
      id: id ?? this.id,
      board: board ?? this.board,
      thread: thread ?? this.thread,
      username: username ?? this.username,
      userid: userid ?? this.userid,
      title: title ?? this.title,
      content: content ?? this.content,
      timestamp: timestamp ?? this.timestamp,
      attachment: attachment ?? this.attachment,
      replyCount: replyCount ?? this.replyCount,
      pinned: pinned ?? this.pinned,
      nsfw: nsfw ?? this.nsfw,
      url: url ?? this.url,
    );
  }

  bool isOP(int? opId) {
    return opId != null && opId == id;
  }

  bool isCommentsEnabled(int? opId) {
    return replyCount > 0 && !isOP(opId);
  }

  bool isThreadOP() {
    return thread == id;
  }

  String pathTo({int? targetId}) {
    if (targetId == null) {
      if (isThreadOP()) {
        return '/$board/$id';
      } else {
        return '/$board/$thread/$id';
      }
    } else if (isThreadOP()) {
      return '/$board/$targetId';
    } else if (targetId == thread) {
      return '/$board/$thread';
    } else {
      return '/$board/$thread/$targetId';
    }
  }
}
