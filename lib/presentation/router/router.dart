import 'package:beamer/beamer.dart';
import 'package:channeler/presentation/screens/feed_screen.dart';
import 'package:channeler/presentation/screens/home_screen.dart';
import 'package:channeler/presentation/screens/not_found_screen.dart';
import 'package:flutter/material.dart';

final routerDelegate = BeamerDelegate(
  initialPath: '/',
  locationBuilder: RoutesLocationBuilder(
    routes: {
      '/': (_, __, ___) => const HomeScreen(title: 'Channeler'),
      '/:board': (context, state, data) => FeedScreen(
            boardName: state.pathParameters['board']!,
          ),
      '/:board/:thread': (context, state, data) {
        return FeedScreen(
          boardName: state.pathParameters['board']!,
          threadId: int.tryParse(state.pathParameters['thread']!) ?? 0,
        );
      },
      '/:board/:thread/:post': (context, state, data) => FeedScreen(
            boardName: state.pathParameters['board']!,
            threadId: int.tryParse(state.pathParameters['thread']!) ?? 0,
            postId: int.tryParse(state.pathParameters['post']!) ?? 0,
          ),
    },
  ).call,
  notFoundPage: const BeamPage(
    title: 'Page Not Found',
    type: BeamPageType.noTransition,
    child: NotFoundScreen(),
  ),
);

RouteInformation onParse(RouteInformation routeInformation) {
  Uri uri = routeInformation.uri;
  List<String> pathSegments = uri.removeFragment().pathSegments.toList();

  if (pathSegments.length == 3 && pathSegments[1] == 'thread') {
    pathSegments.removeAt(1);
    if (uri.hasFragment) {
      String postId = uri.fragment.substring(1);
      String threadId = pathSegments.last;

      if (threadId != postId) {
        pathSegments.add(postId);
      }
    }

    uri = Uri.parse('/${pathSegments.join('/')}');
  }

  return RouteInformation(
    uri: Uri.parse(uri.path),
    state: routeInformation.state,
  );
}
