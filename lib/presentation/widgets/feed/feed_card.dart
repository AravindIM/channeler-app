import 'package:channeler/data/models/post.dart';
import 'package:channeler/presentation/widgets/feed/feed_card_text_body.dart';
import 'package:channeler/presentation/widgets/feed/feed_card_footer.dart';
import 'package:channeler/presentation/widgets/feed/feed_card_header.dart';
import 'package:channeler/presentation/widgets/media/flick_multi_player/flick_multi_manager.dart';
import 'package:channeler/presentation/widgets/media/media_handler.dart';
import 'package:flutter/material.dart';

class FeedCard extends StatelessWidget {
  const FeedCard({
    super.key,
    required this.post,
    required this.flickMultiManager,
    this.opId,
  });
  final Post post;
  final FlickMultiManager flickMultiManager;
  final int? opId;

  @override
  Widget build(BuildContext context) {
    final colorScheme = Theme.of(context).colorScheme;
    final String attachment = post.attachment ?? '';

    return Card(
      clipBehavior: Clip.antiAliasWithSaveLayer,
      surfaceTintColor: colorScheme.surface,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          FeedCardHeader(
            post: post,
            padding: const EdgeInsets.fromLTRB(20, 20, 20, 10),
          ),
          const Divider(
            thickness: 0.2,
            color: Colors.grey,
            height: 0.2,
          ),
          if (attachment.isNotEmpty)
            MediaHandler(
              mediaUrl: attachment,
              flickMultiManager: flickMultiManager,
            ),
          if (attachment.isNotEmpty)
            const Divider(
              thickness: 0.2,
              color: Colors.grey,
              height: 0.2,
            ),
          if (attachment.isNotEmpty) const SizedBox(height: 20),
          FeedCardTextBody(
            post: post,
            padding: const EdgeInsets.fromLTRB(20, 5, 20, 5),
            opId: opId,
          ),
          FeedCardFooter(
            post: post,
            padding: const EdgeInsets.fromLTRB(20, 5, 20, 5),
            opId: opId,
          )
        ],
      ),
    );
  }
}
