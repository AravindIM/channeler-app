import 'package:beamer/beamer.dart';
import 'package:channeler/data/models/post.dart';
import 'package:flutter/material.dart';
import 'package:share_plus/share_plus.dart';

class FeedCardFooter extends StatelessWidget {
  const FeedCardFooter({
    super.key,
    required this.post,
    required this.padding,
    this.opId,
  });
  final Post post;
  final EdgeInsets padding;
  final int? opId;

  @override
  Widget build(BuildContext context) {
    const iconSize = 20.0;
    return Padding(
      padding: padding,
      child: Row(
        children: [
          IconButton(
            icon: const Icon(Icons.favorite_outline),
            iconSize: iconSize,
            onPressed: () {},
          ),
          IconButton(
            icon: const Icon(Icons.share_outlined),
            iconSize: iconSize,
            onPressed: () {
              Share.share(post.url);
            },
          ),
          IconButton(
            icon: Row(
              children: [
                const Icon(Icons.comment_outlined),
                const SizedBox(width: 5),
                Text(post.replyCount.toString())
              ],
            ),
            iconSize: iconSize,
            onPressed: post.isCommentsEnabled(opId)
                ? () {
                    context.beamToNamed(post.pathTo());
                  }
                : null,
          ),
        ],
      ),
    );
  }
}
