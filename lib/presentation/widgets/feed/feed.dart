import 'package:channeler/data/models/board.dart';
import 'package:channeler/data/models/post.dart';
import 'package:channeler/presentation/widgets/feed/feed_card.dart';
import 'package:channeler/presentation/widgets/media/flick_multi_player/flick_multi_manager.dart';
import 'package:flutter/material.dart';
import 'package:visibility_detector/visibility_detector.dart';

class Feed extends StatelessWidget {
  Feed({
    super.key,
    required this.posts,
    required this.board,
    required this.refreshFunction,
    ScrollController? scrollController,
    this.opId,
  }) : scrollController = scrollController ?? ScrollController();

  final Board board;
  final List<Post> posts;
  final Function refreshFunction;
  final ScrollController scrollController;
  final int? opId;
  final FlickMultiManager flickMultiManager = FlickMultiManager();

  @override
  Widget build(BuildContext context) {
    return VisibilityDetector(
      key: ObjectKey(flickMultiManager),
      onVisibilityChanged: (visibility) {
        if (visibility.visibleFraction == 0) {
          flickMultiManager.pause();
        }
      },
      child: RefreshIndicator(
        onRefresh: () => Future.sync(() => refreshFunction),
        child: Scrollbar(
          controller: scrollController,
          interactive: true,
          child: ListView.builder(
            controller: scrollController,
            padding: EdgeInsets.zero,
            itemCount: posts.length,
            itemBuilder: (context, index) {
              final post = posts[index];
              return FeedCard(
                post: post,
                flickMultiManager: flickMultiManager,
                opId: opId,
              );
            },
          ),
        ),
      ),
    );
  }
}
