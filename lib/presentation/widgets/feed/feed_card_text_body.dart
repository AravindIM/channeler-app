import 'package:beamer/beamer.dart';
import 'package:channeler/data/models/post.dart';
import 'package:channeler/presentation/widgets/media/image_handler.dart';
import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:url_launcher/url_launcher.dart';

class FeedCardTextBody extends StatelessWidget {
  const FeedCardTextBody({
    super.key,
    required this.post,
    required this.padding,
    this.opId,
  });
  final Post post;
  final EdgeInsets padding;
  final int? opId;

  @override
  Widget build(BuildContext context) {
    final colorScheme = Theme.of(context).colorScheme;

    return Padding(
      padding: padding,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (post.title.isNotEmpty)
            Text(
              post.title,
              softWrap: true,
              style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
          if (post.title.isNotEmpty) const SizedBox(height: 10),
          Markdown(
            padding: EdgeInsets.zero,
            data: post.content,
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            styleSheet:
                MarkdownStyleSheet.fromTheme(Theme.of(context)).copyWith(
              blockquotePadding: const EdgeInsets.all(20),
              blockquoteDecoration: BoxDecoration(
                color: colorScheme.primary.withOpacity(0.1),
                borderRadius: BorderRadius.circular(5),
              ),
              a: TextStyle(
                color: colorScheme.primary,
                decoration: TextDecoration.underline,
                decorationColor: colorScheme.primary,
              ),
              listBullet: TextStyle(
                color: colorScheme.primary,
              ),
            ),
            imageBuilder: (uri, title, alt) {
              return ImageHandler(imageUrl: uri.toString());
            },
            onTapLink: (text, href, title) {
              String url = href ?? '';
              final tagRegex = RegExp(r'^>>(\d+)$');
              final boardTagRegex = RegExp(r'^>>>\/([A-Za-z]+)\/[A-Za-z]*$');
              final defaultTagRegex = RegExp(r'^[>]+[\S]+$');

              if (tagRegex.hasMatch(text)) {
                final tagPostId =
                    int.tryParse(tagRegex.firstMatch(text)!.group(1)!) ?? 0;

                if (tagPostId != opId) {
                  final path = post.pathTo(targetId: tagPostId);
                  context.beamToNamed(path, beamBackOnPop: true);
                }
              } else if (boardTagRegex.hasMatch(text)) {
                final boardName = boardTagRegex.firstMatch(text)!.group(1)!;
                if (post.board != boardName) {
                  context.beamToNamed('/$boardName', beamBackOnPop: true);
                }
              } else if (url.isNotEmpty && !defaultTagRegex.hasMatch(text)) {
                launchUrl(
                  Uri.parse(url),
                  mode: LaunchMode.externalApplication,
                );
              }
            },
          ),
        ],
      ),
    );
  }
}
