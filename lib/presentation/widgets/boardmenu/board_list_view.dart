import 'package:beamer/beamer.dart';
import 'package:channeler/logic/cubit/board_expansion_tile_cubit.dart';
import 'package:channeler/logic/cubit/boards_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:channeler/data/models/board.dart';
import 'package:flutter/material.dart';

class BoardListView extends StatelessWidget {
  final String currentBoard;

  const BoardListView({
    super.key,
    required this.currentBoard,
  });

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BoardsCubit, BoardsState>(
      builder: (_, state) {
        Widget child;
        switch (state) {
          case BoardsInitial():
            context.read<BoardsCubit>().fetchBoards();
            child = ListView(
              padding: EdgeInsets.zero,
              children: const [
                ListTile(
                  leading: CircularProgressIndicator(),
                  title: Text(
                    'Loading list of boards! Please wait!',
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              ],
            );
          case BoardsLoading():
            child = ListView(
              padding: EdgeInsets.zero,
              children: const [
                ListTile(
                  leading: CircularProgressIndicator(),
                  title: Text(
                    'Loading list of boards! Please wait!',
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              ],
            );
          case BoardsSuccess():
            final List<Board> boards = state.boards;
            final List<Board> sfwBoards =
                boards.where((board) => !board.nsfw).toList();
            final List<Board> nsfwBoards =
                boards.where((board) => board.nsfw).toList();
            final colorScheme = Theme.of(context).colorScheme;
            final nsfwColor = colorScheme.error;
            child =
                BlocBuilder<BoardExpansionTileCubit, BoardExpansionTileState>(
              builder: (_, state) {
                return ListView(
                  padding: EdgeInsets.zero,
                  children: [
                    ExpansionTile(
                      initiallyExpanded: state.sfwState,
                      onExpansionChanged: (value) {
                        context
                            .read<BoardExpansionTileCubit>()
                            .changeSfwState(value);
                      },
                      title: Text(
                        "General (SFW)",
                        style: TextStyle(
                            fontSize: 16,
                            color: colorScheme.primary,
                            fontWeight: FontWeight.w700),
                      ),
                      children: _boardListTiles(context, sfwBoards),
                    ),
                    ExpansionTile(
                      initiallyExpanded: state.nsfwState,
                      onExpansionChanged: (value) {
                        context
                            .read<BoardExpansionTileCubit>()
                            .changeNsfwState(value);
                      },
                      title: Text(
                        "Adult (NSFW 18+)",
                        style: TextStyle(
                            fontSize: 16,
                            color: nsfwColor,
                            fontWeight: FontWeight.w700),
                      ),
                      children: _boardListTiles(context, nsfwBoards),
                    ),
                  ],
                );
              },
            );
          case BoardsFailed():
            child = ListView(
              padding: EdgeInsets.zero,
              children: const [
                ListTile(
                  leading: Icon(Icons.error_outline),
                  title: Text(
                    'Sorry, we could not fetch boards! Try checking if you are connected to the internet',
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              ],
            );
        }
        return Expanded(
          child: child,
        );
      },
    );
  }

  List<Widget> _boardListTiles(BuildContext context, List<Board> boards) {
    return boards.map(
      (board) {
        final themeData = Theme.of(context);
        final colorScheme = Theme.of(context).colorScheme;
        final nsfwColor = colorScheme.error;
        final onNsfwColor = colorScheme.onError;
        final onPrimary = colorScheme.onPrimary;
        final boardName = board.name;
        final boardTitle = board.title;
        final bool isSelected = boardName == currentBoard;
        return ListTile(
          selected: isSelected,
          selectedTileColor: board.nsfw
              ? nsfwColor.withOpacity(0.3)
              : themeData.listTileTheme.selectedTileColor,
          leading: CircleAvatar(
            backgroundColor: board.nsfw ? nsfwColor : colorScheme.primary,
            child: FittedBox(
              fit: BoxFit.scaleDown,
              child: Padding(
                padding: const EdgeInsets.all(8),
                child: Text(
                  '/$boardName/',
                  style: TextStyle(
                      fontWeight: FontWeight.w900,
                      color: board.nsfw ? onNsfwColor : onPrimary),
                ),
              ),
            ),
          ),
          title: Text(boardTitle),
          onTap: () {
            context.beamToNamed('/$boardName');
            Navigator.of(context).pop();
          },
        );
      },
    ).toList();
  }
}
