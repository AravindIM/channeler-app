import 'package:beamer/beamer.dart';
import 'package:channeler/data/models/board.dart';
import 'package:channeler/logic/cubit/boards_cubit.dart';
import 'package:channeler/logic/cubit/feed_cubit.dart';
import 'package:channeler/logic/cubit/scroll_to_top_visibility_cubit.dart';
import 'package:channeler/presentation/widgets/boardmenu/boardmenu.dart';
import 'package:channeler/presentation/widgets/feed/feed.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FeedPostWithoutThreadId implements Exception {}

class FeedScreen extends StatelessWidget {
  final String boardName;
  final int? threadId;
  final int? postId;
  final feedScrollController = ScrollController();
  final feedCubit = FeedCubit();
  final scrollToTopVisibilityCubit = ScrollToTopVisibilityCubit();
  FeedScreen({super.key, required this.boardName, this.threadId, this.postId}) {
    if (threadId == null && postId != null) {
      throw FeedPostWithoutThreadId();
    }
  }

  void _feedScrollListener() {
    if (feedScrollController.hasClients) {
      final currentPosition = feedScrollController.position.pixels;
      final maxScrollExtend = feedScrollController.position.maxScrollExtent -
          feedScrollController.position.minScrollExtent;
      final scrollPercent = currentPosition / maxScrollExtend;
      final direction = feedScrollController.position.userScrollDirection;

      if (direction == ScrollDirection.forward && scrollPercent >= 0.005) {
        scrollToTopVisibilityCubit.show();
      } else {
        scrollToTopVisibilityCubit.hide();
      }
    }
  }

  Widget _loadingWidget({String? title, String? message}) {
    final pageTitle = title ?? "Loading";
    final loadingMessage = message ?? "Loading posts... Please wait!";
    return NestedScrollView(
      headerSliverBuilder: (context, innerBoxIsScrolled) => [
        SliverAppBar(
          floating: true,
          snap: true,
          foregroundColor: Theme.of(context).colorScheme.primary,
          title: Text(pageTitle),
        ),
      ],
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          const CircularProgressIndicator(),
          const SizedBox(height: 30),
          Padding(
            padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
            child: Text(
              loadingMessage,
              style: const TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.w300,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _errorWidget(BuildContext context, {String? title}) {
    final pageTitle = title ?? 'Oops';
    final String path = Beamer.of(context).configuration.uri.toString();
    final message1 = '> Sorry, could not load posts for $path !';
    const message2 = '> Try checking if the path is correct';
    const message3 =
        '> Check your internet connection to see if you are offline';
    return NestedScrollView(
      headerSliverBuilder: (context, innerBoxIsScrolled) => [
        SliverAppBar(
          floating: true,
          snap: true,
          foregroundColor: Theme.of(context).colorScheme.primary,
          title: Text(pageTitle),
        ),
      ],
      body: Padding(
        padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              '404',
              style: TextStyle(
                fontSize: 100,
                color: Theme.of(context).colorScheme.primary,
                fontWeight: FontWeight.w900,
              ),
            ),
            const SizedBox(height: 30),
            Text(
              message1,
              style: const TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.w300,
              ),
            ),
            const Text(
              message2,
              style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.w300,
              ),
            ),
            const Text(
              message3,
              style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.w300,
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    feedScrollController.addListener(_feedScrollListener);

    String pageTitle = postId != null
        ? (postId == 0 ? "Loading" : ">>$postId")
        : threadId != null
            ? (threadId == 0 ? "Loading" : ">>$threadId")
            : "/$boardName/";

    return Scaffold(
      drawer: BoardMenu(
        currentBoard: boardName,
      ),
      body: BlocBuilder<BoardsCubit, BoardsState>(
        builder: (_, boardState) {
          switch (boardState) {
            case BoardsInitial():
              context.read<BoardsCubit>().fetchBoards();
              return _loadingWidget(title: pageTitle);
            case BoardsLoading():
              return _loadingWidget(title: pageTitle);
            case BoardsSuccess():
              try {
                Board board =
                    Board.findBoardByName(boardState.boards, boardName);
                if (postId == null && threadId == null) {
                  pageTitle = board.title;
                }
                return BlocBuilder<FeedCubit, FeedState>(
                  bloc: feedCubit,
                  builder: (_, feedState) {
                    final refreshFunction = postId != null
                        ? () => feedCubit.refreshCommentPosts(
                            board, threadId!, postId!)
                        : threadId != null
                            ? () =>
                                feedCubit.refreshThreadPosts(board, threadId!)
                            : () => feedCubit.refreshCatalogPosts(board);
                    switch (feedState) {
                      case FeedInitial():
                        refreshFunction();
                        return _loadingWidget(title: pageTitle);
                      case FeedLoading():
                        return _loadingWidget(title: pageTitle);
                      case FeedSuccess():
                        return NestedScrollView(
                          headerSliverBuilder: (context, innerBoxIsScrolled) =>
                              [
                            SliverAppBar(
                              floating: true,
                              snap: true,
                              foregroundColor:
                                  Theme.of(context).colorScheme.primary,
                              title: Text(pageTitle),
                            ),
                          ],
                          body: Feed(
                            posts: feedState.posts,
                            board: board,
                            scrollController: feedScrollController,
                            refreshFunction: refreshFunction,
                            opId: postId ?? threadId,
                          ),
                        );
                      case FeedFailed():
                        return _errorWidget(context);
                    }
                  },
                );
              } on BoardNotFound catch (_) {
                return _errorWidget(context);
              }
            case BoardsFailed():
              return _errorWidget(context);
          }
        },
      ),
      floatingActionButton: BlocBuilder<ScrollToTopVisibilityCubit, bool>(
        bloc: scrollToTopVisibilityCubit,
        builder: (context, state) {
          return AnimatedSlide(
            duration: const Duration(milliseconds: 200),
            offset: state ? Offset.zero : const Offset(0, 2),
            curve: Curves.easeInOut,
            child: AnimatedOpacity(
              opacity: state ? 1 : 0,
              duration: const Duration(milliseconds: 100),
              curve: Curves.easeInOut,
              child: FloatingActionButton(
                child: const Icon(Icons.arrow_upward),
                onPressed: () {
                  if (feedScrollController.hasClients) {
                    final position =
                        feedScrollController.position.minScrollExtent;
                    scrollToTopVisibilityCubit.hide();
                    feedScrollController.animateTo(
                      position,
                      duration: const Duration(seconds: 1),
                      curve: Curves.easeInOut,
                    );
                  }
                },
              ),
            ),
          );
        },
      ),
    );
  }
}
