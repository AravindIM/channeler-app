import 'package:beamer/beamer.dart';
import 'package:channeler/presentation/widgets/boardmenu/boardmenu.dart';
import 'package:flutter/material.dart';

class NotFoundScreen extends StatelessWidget {
  final String title;
  const NotFoundScreen({super.key, String? title}) : title = title ?? "Oops!";

  @override
  Widget build(BuildContext context) {
    final String path = Beamer.of(context).configuration.uri.toString();
    final message1 = '> Sorry, could not load posts for $path !';
    const message2 = '> Try checking if the path is correct';
    const message3 =
        '> Check your internet connection to see if you are offline';

    return Scaffold(
      drawer: const BoardMenu(currentBoard: ''),
      body: NestedScrollView(
        headerSliverBuilder: (context, innerBoxIsScrolled) => [
          SliverAppBar(
            floating: true,
            snap: true,
            foregroundColor: Theme.of(context).colorScheme.primary,
            title: Text(title),
          ),
        ],
        body: Padding(
          padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                '404',
                style: TextStyle(
                  fontSize: 100,
                  color: Theme.of(context).colorScheme.primary,
                  fontWeight: FontWeight.w900,
                ),
              ),
              const SizedBox(height: 30),
              Text(
                message1,
                style: const TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.w300,
                ),
              ),
              const Text(
                message2,
                style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.w300,
                ),
              ),
              const Text(
                message3,
                style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.w300,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
