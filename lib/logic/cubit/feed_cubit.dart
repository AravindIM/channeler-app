import 'package:channeler/data/models/board.dart';
import 'package:channeler/data/repositories/fourchan_respository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:channeler/data/models/post.dart';
import 'package:equatable/equatable.dart';

part 'feed_state.dart';

class FeedCubit extends Cubit<FeedState> {
  final FourChanRepository _fourChanRepository;

  FeedCubit({FourChanRepository? fourChanRepository})
      : _fourChanRepository = fourChanRepository ?? FourChanRepository(),
        super(FeedInitial());

  void refreshCatalogPosts(Board board) async {
    emit(FeedLoading());
    try {
      final posts = await _fourChanRepository.getCatalogPosts(board);
      if (posts.isNotEmpty) {
        emit(FeedSuccess(posts));
      } else {
        emit(FeedFailed());
      }
    } on Exception {
      emit(FeedFailed());
    }
  }

  void refreshThreadPosts(Board board, int threadId) async {
    emit(FeedLoading());
    try {
      final posts = await _fourChanRepository.getThreadPosts(board, threadId);
      if (posts.isNotEmpty) {
        emit(FeedSuccess(posts));
      } else {
        emit(FeedFailed());
      }
    } on Exception {
      emit(FeedFailed());
    }
  }

  void refreshCommentPosts(Board board, int threadId, int postId) async {
    emit(FeedLoading());
    try {
      final threadPosts =
          await _fourChanRepository.getThreadPosts(board, threadId);
      final commentPosts =
          _fourChanRepository.getCommentPosts(threadPosts, postId);
      if (commentPosts.isNotEmpty) {
        emit(FeedSuccess(commentPosts));
      } else {
        emit(FeedFailed());
      }
    } on Exception {
      emit(FeedFailed());
    }
  }
}
