part of 'feed_cubit.dart';

sealed class FeedState extends Equatable {
  const FeedState();

  @override
  List<Object> get props => [];
}

final class FeedInitial extends FeedState {}

final class FeedLoading extends FeedState {}

final class FeedFailed extends FeedState {}

final class FeedSuccess extends FeedState {
  final List<Post> posts;

  const FeedSuccess(this.posts);
  @override
  List<Object> get props => [posts];
}
