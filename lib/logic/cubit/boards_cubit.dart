import 'package:channeler/data/repositories/fourchan_respository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:channeler/data/models/board.dart';
import 'package:equatable/equatable.dart';

part 'boards_state.dart';

class BoardsCubit extends Cubit<BoardsState> {
  final FourChanRepository _fourChanRepository = FourChanRepository();

  BoardsCubit() : super(BoardsInitial());

  void fetchBoards() async {
    switch (state) {
      case BoardsInitial():
      case BoardsFailed():
        emit(BoardsLoading());
        try {
          final boards = await _fourChanRepository.getBoards();
          emit(BoardsSuccess(boards));
        } on Exception {
          emit(BoardsFailed());
        }
        break;
      default:
    }
  }
}
