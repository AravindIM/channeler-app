part of 'boards_cubit.dart';

sealed class BoardsState extends Equatable {
  const BoardsState();

  @override
  List<Object> get props => [];
}

final class BoardsInitial extends BoardsState {}

final class BoardsLoading extends BoardsState {}

final class BoardsFailed extends BoardsState {}

final class BoardsSuccess extends BoardsState {
  final List<Board> boards;

  const BoardsSuccess(this.boards);

  @override
  List<Object> get props => [boards];
}
