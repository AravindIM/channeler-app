// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'board_expansion_tile_cubit.dart';

class BoardExpansionTileState extends Equatable {
  final bool sfwState;
  final bool nsfwState;

  const BoardExpansionTileState(
      {required this.sfwState, required this.nsfwState});

  @override
  List<Object> get props => [sfwState, nsfwState];

  BoardExpansionTileState copyWith({
    bool? sfwState,
    bool? nsfwState,
  }) {
    return BoardExpansionTileState(
      sfwState: sfwState ?? this.sfwState,
      nsfwState: nsfwState ?? this.nsfwState,
    );
  }
}
