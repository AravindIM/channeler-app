import 'package:flutter_bloc/flutter_bloc.dart';

class ScrollToTopVisibilityCubit extends Cubit<bool> {
  ScrollToTopVisibilityCubit() : super(false);

  void show() {
    emit(true);
  }

  void hide() {
    emit(false);
  }
}
