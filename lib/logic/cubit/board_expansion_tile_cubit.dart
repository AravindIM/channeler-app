import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';

part 'board_expansion_tile_state.dart';

class BoardExpansionTileCubit extends Cubit<BoardExpansionTileState> {
  BoardExpansionTileCubit()
      : super(const BoardExpansionTileState(sfwState: false, nsfwState: false));

  void changeSfwState(bool value) {
    emit(state.copyWith(sfwState: value));
  }

  void changeNsfwState(bool value) {
    emit(state.copyWith(nsfwState: value));
  }
}
